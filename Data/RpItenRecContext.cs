using Microsoft.EntityFrameworkCore;

namespace RpIten.Data{
    public class RpItenRecContext :DbContext{
        public RpItenRecContext (
            DbContextOptions<RpItenRecContext> options)
            : base(options)
            {
        }

        public DbSet<RpIten.Models.Rec> Rec {get; set;}
    }


}