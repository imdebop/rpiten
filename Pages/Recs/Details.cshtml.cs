using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RpIten.Data;
using RpIten.Models;

namespace RpIten.Pages.Recs
{
    public class DetailsModel : PageModel
    {
        private readonly RpIten.Data.RpItenRecContext _context;

        public DetailsModel(RpIten.Data.RpItenRecContext context)
        {
            _context = context;
        }

        public Rec Rec { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Rec = await _context.Rec.FirstOrDefaultAsync(m => m.ID == id);

            if (Rec == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
