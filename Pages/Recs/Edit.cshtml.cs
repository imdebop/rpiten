using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RpIten.Data;
using RpIten.Models;

namespace RpIten.Pages.Recs
{
    public class EditModel : PageModel
    {
        private readonly RpIten.Data.RpItenRecContext _context;

        public EditModel(RpIten.Data.RpItenRecContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Rec Rec { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Rec = await _context.Rec.FirstOrDefaultAsync(m => m.ID == id);

            if (Rec == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Rec).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RecExists(Rec.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool RecExists(int id)
        {
            return _context.Rec.Any(e => e.ID == id);
        }
    }
}
