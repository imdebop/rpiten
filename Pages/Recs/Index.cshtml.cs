using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RpIten.Data;
using RpIten.Models;

namespace RpIten.Pages.Recs
{
    public class IndexModel : PageModel
    {
        private readonly RpIten.Data.RpItenRecContext _context;

        public IndexModel(RpIten.Data.RpItenRecContext context)
        {
            _context = context;
        }

        public IList<Rec> Rec { get;set; }

        public async Task OnGetAsync()
        {
            Rec = await _context.Rec.ToListAsync();
        }
    }
}
