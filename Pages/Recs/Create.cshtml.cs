using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using RpIten.Data;
using RpIten.Models;

namespace RpIten.Pages.Recs
{
    public class CreateModel : PageModel
    {
        private readonly RpIten.Data.RpItenRecContext _context;

        public CreateModel(RpIten.Data.RpItenRecContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Rec Rec { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Rec.Add(Rec);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
