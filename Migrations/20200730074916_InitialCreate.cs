﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RpIten.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Rec",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SeiriNo = table.Column<string>(nullable: true),
                    RosenNo = table.Column<string>(nullable: true),
                    TateNo = table.Column<string>(nullable: true),
                    ShiTateNo = table.Column<string>(nullable: true),
                    Youto = table.Column<string>(nullable: true),
                    Men = table.Column<decimal>(nullable: false),
                    Koho = table.Column<string>(nullable: true),
                    JigyoGaku = table.Column<decimal>(nullable: false),
                    Shimei = table.Column<string>(nullable: true),
                    Detail = table.Column<string>(nullable: true),
                    KanendoSum = table.Column<decimal>(nullable: false),
                    ChosaGaku = table.Column<decimal>(nullable: false),
                    Note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rec", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Rec");
        }
    }
}
