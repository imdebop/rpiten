using System;
using System.ComponentModel.DataAnnotations;

namespace RpIten.Models{
    public class Rec{
        public int ID {get; set;}
        [Display(Name = "整理番号")]
        public string SeiriNo {get; set;}
        public string RosenNo {get; set;}
        public string TateNo {get; set;} 
        public string ShiTateNo {get; set;}
        public string Youto {get; set;}
        public decimal Men {get; set;}
        public string Koho {get; set;}
        public decimal JigyoGaku {get; set;}
        public string Shimei {get; set;}
        public string Detail {get; set;}
        public decimal KanendoSum {get; set;}
        public decimal ChosaGaku {get; set;}
        public string Note {get; set;}
    }
    



}