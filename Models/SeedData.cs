using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RpIten.Data;
using System;
using System.Linq;

namespace RpIten.Models{
    public static class SeedData{
        public static void Initialize(IServiceProvider serviceProvider){
            using(var context = new RpItenRecContext(
                serviceProvider.GetRequiredService<DbContextOptions<RpItenRecContext>>()
            )){
                //Look for any recs
                if (context.Rec.Any()){
                    return; //Db has been seeded
                }

                context.Rec.AddRange(
                    new Rec{
                        SeiriNo = "1.01",
                        RosenNo = "一色高洲線",
                        Detail = "2017:地活(直):25,010,501"
                    },                    
                    new Rec{
                        SeiriNo = "4.03",
                        RosenNo = "一色高洲線",
                        Detail = "2005:地活(直):21,280,000"
                    }    

                );
                
                context.SaveChanges();
            }
        }
    }


}